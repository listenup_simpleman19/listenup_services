#!/bin/sh

export LISTENUP_APP_CONFIG=production

/usr/local/bin/gunicorn --config /usr/src/app/gunicorn_config.py wsgi:app