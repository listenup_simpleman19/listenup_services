FROM python:3.7

ARG appRoot=/usr/src/app

RUN mkdir -p ${appRoot}
RUN mkdir -p ${appRoot}/.wheels
WORKDIR ${appRoot}

COPY requirements.txt /usr/src/app/
RUN pip install -r requirements.txt

COPY requirements-listenup.txt /usr/src/app/
COPY .wheels /usr/src/app/.wheels
RUN pip install --find-links .wheels --no-cache-dir -r requirements-listenup.txt

ARG SERVICE_NAME
ARG SERVICE_VERSION
ENV SERVICE_NAME $SERVICE_NAME
ENV SERVICE_VERSION $SERVICE_VERSION

COPY . ${appRoot}

EXPOSE 7560

# Start gunicorn
CMD ["./entrypoint.sh"]
