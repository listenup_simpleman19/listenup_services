import os
from flask import Flask
from config import config


def create_app(config_name=None):
    if config_name is None:
        config_name = os.environ.get('LISTENUP_APP_CONFIG', 'development')
        print("Starting up in: " + config_name)
    app = Flask(__name__)
    app.config.from_object(config[config_name])

    # Register web application routes
    from .routes import main as main_blueprint
    app.register_blueprint(main_blueprint)

    return app
