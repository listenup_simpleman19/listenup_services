import os
from flask import Blueprint, render_template, session, redirect, url_for, request, current_app as app, g

from listenup_common.api_response import ApiException, ApiMessage, ApiResult
import datetime
import docker
from docker.errors import APIError

main = Blueprint('main', __name__, url_prefix='/api/services')

root_path = os.path.dirname(os.path.abspath(__file__))


@main.route('/nodes', methods=['GET'])
def nodes():
    client = docker.APIClient(base_url='unix://var/run/docker.sock')

    filters = {}
    if request.args.get("role"):
        filters['role'] = request.args.get("role")
    if request.args.get("id"):
        filters['id'] = request.args.get("id")
    if request.args.get("name"):
        filters['name'] = request.args.get("name")

    try:
        nodes = [node_to_dict(node, client) for node in client.nodes(filters=filters)]
    except APIError as e:
        raise ApiException(message=str(e), status=503)

    labels_args = request.args.get("labels")
    labels = {}
    nodes_to_return = []
    if labels_args:
        for label in labels_args.split(","):
            splitLabel = label.split(":")
            labels[splitLabel[0]] = splitLabel[1]
        for node in nodes:
            nodeLabels = node['labels']
            for k, v in labels.items():
                if k in nodeLabels and nodeLabels.get(k) == v and node not in nodes_to_return:
                    nodes_to_return.append(node)

    else:
        nodes_to_return = nodes

    return ApiResult(value=nodes_to_return).to_response()


def service_to_task_service(task, service):
    return {
        'id': task['ID'],
        'service_id': task['ServiceID'],
        'node_id': task['NodeID'],
        'service': services_to_dict(service),
    }


def node_to_dict(node, client):
    tasks = client.tasks(filters={'node': node["ID"], 'desired-state': 'running'})
    task_services = [service_to_task_service(task, client.inspect_service(task['ServiceID'])) for task in tasks]
    return {
        'id': node["ID"],
        'tasks_services': task_services,
        'engine_version': node["Description"]["Engine"]["EngineVersion"],
        'hostname': node["Description"]["Hostname"],
        'platform': node["Description"]["Platform"],
        'labels': node["Spec"].get("Labels",[]),
        'availability': node["Spec"]["Availability"],
        'role': node["Spec"]["Role"],
        'ip': node["Status"]["Addr"],
        'state': node["Status"]["State"],
    }


@main.route('/services')
def services():
    client = docker.APIClient(base_url='unix://var/run/docker.sock')

    filters = {}
    if request.args.get("label"):
        filters['label'] = request.args.get("label")
    if request.args.get("id"):
        filters['id'] = request.args.get("id")
    if request.args.get("name"):
        filters['name'] = request.args.get("name")
    if request.args.get("mode"):
        filters['mode'] = request.args.get("mode")

    try:
        services = [services_to_dict(service) for service in client.services(filters=filters)]
    except APIError as e:
        raise ApiException(message=str(e), status=503)
    return ApiResult(value=services, status=200).to_response()


def services_to_dict(service):
    return {
        'id': service['ID'],
        'name': service["Spec"]["Name"],
        'ips': service["Endpoint"].get("VirtualIPs", []),
        'ports': service["Endpoint"].get("Ports", []),
        'image': service["Spec"]["TaskTemplate"]["ContainerSpec"]["Image"],
        'mounts': service["Spec"]["TaskTemplate"]["ContainerSpec"].get("Mounts", []),
        'labels': service["Spec"]["Labels"],
    }


@main.errorhandler(ApiException)
def handle_api_exception(error: ApiException):
    return error.to_result().to_response()
