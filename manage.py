import os
import subprocess
import contextlib

from flask import current_app as app
from flask_script import Manager, Server

from services import create_app

manager = Manager(create_app)

root_path = os.path.dirname(os.path.abspath(__file__))


def _make_context():
    return dict(app=manager.app)


@manager.command
def test():
    """Run unit tests"""
    tests = subprocess.Popen(['python', '-m', 'unittest'])
    tests.wait()
    with contextlib.suppress(FileNotFoundError):
        os.remove('files/test.db')


if __name__ == '__main__':
    manager.run(commands={"runserver": Server(host="0.0.0.0", port=7560)})
