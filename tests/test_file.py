import unittest
from files.models import File
from config import TestingConfig
import os


class TestFile(unittest.TestCase):
    def setUp(self):
        pass

    def test_object(self):
        file = File(file_name="test.mp3", location=TestingConfig.FILES_PATH)
        self.assertIsNotNone(file.guid_file_name)
        self.assertIsNotNone(file.file_extension)
        self.assertEqual("mp3", file.file_extension)
        self.assertIsNotNone(file.original_file_name)
        self.assertEqual("test", file.original_file_name)
        self.assertEqual(TestingConfig.FILES_PATH, file.location)
        self.assertEqual(os.path.join(TestingConfig.FILES_PATH, file.guid_file_name + ".mp3"), file.path)
        self.assertEqual(os.path.join(TestingConfig.FILES_PATH, file.guid_file_name + ".meta"), file.metapath)

        file = File(file_name="t.e.s.t.mp3", location=TestingConfig.FILES_PATH)
        self.assertIsNotNone(file.guid_file_name)
        self.assertIsNotNone(file.file_extension)
        self.assertEqual("mp3", file.file_extension)
        self.assertIsNotNone(file.original_file_name)
        self.assertEqual("t.e.s.t", file.original_file_name)
        self.assertEqual(TestingConfig.FILES_PATH, file.location)
        self.assertEqual(os.path.join(TestingConfig.FILES_PATH, file.guid_file_name + ".mp3"), file.path)
        self.assertEqual(os.path.join(TestingConfig.FILES_PATH, file.guid_file_name + ".meta"), file.metapath)
